package main

import (
	"os"

	"cloudmicro/platform/auth-manager/internal/handler"

	wb "gitlab.com/cloudmicro/platform/microservices/cm-http.git/pkg/webserver"
)

func main() {

	port := os.Getenv("AUTH_MGR_PORT")
	server := &wb.WebServer{}

	server.AddHandler("/authm/validate/access", "POST", handler.ValidateAccessHandler)
	server.AddHandler("/authm/auth", "POST", handler.AuthenticateHandler)
	server.AddHandler("/authm/refresh/auth", "POST", handler.RefreshTokenHandler)
	server.AddHandler("/authm/org/select", "POST", handler.SelectOrgHandler)

	if port == "" {

		port = "8080"
	}

	server.StartUp(port)
}
