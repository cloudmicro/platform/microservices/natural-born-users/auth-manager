package handler

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/valyala/fasthttp"

	"cloudmicro/platform/auth-manager/internal/orm"
	"cloudmicro/platform/auth-manager/internal/token"
	"cloudmicro/platform/auth-manager/internal/util"

	usr "gitlab.com/cloudmicro/platform/microservices/natural-born-users/iam-models.git/pkg/user"
)

//ValidateAccessHandler ...
func ValidateAccessHandler(ctx *fasthttp.RequestCtx) {

	ctx.SetContentType("application/json; charset=UTF-8")

	access := string(ctx.Request.Header.Peek("X-CMAK-ID"))
	secret := string(ctx.Request.Header.Peek("X-CMAK-SECRET"))
	jwtInCookie := string(ctx.Request.Header.Cookie("X-CMCSRF-TOKEN"))
	jwtInHeader := string(ctx.Request.Header.Peek("X-CMA-TOKEN"))

	if jwtInHeader != "" && jwtInCookie != "" {

		if _, err := token.ValidateJWT(jwtInHeader, jwtInCookie); err != nil {

			ctx.SetBody([]byte(`{"message": "JWT not valid or expired"}`))
			ctx.SetStatusCode(fasthttp.StatusUnauthorized)
			return
		}
		//FUTURE FEAT: ValidateActionAgainstRole()
	} else if access != "" && secret != "" {

		if err := orm.ValidateAccessTokens(access, secret); err != nil {

			ctx.SetBody([]byte(`{"message": "access and secret not valid"}`))
			ctx.SetStatusCode(fasthttp.StatusUnauthorized)
			return
		}
		//FUTURE FEAT: ValidateActionAgainstRole()
	} else {

		ctx.SetBody([]byte(`{"message": "error reading request"}`))
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		return

	}

	ctx.Response.Header.Add("X-CMDB", time.Now().String())
	ctx.SetStatusCode(fasthttp.StatusOK)
	ctx.SetBody([]byte(`{"message": "crendentials are valid"}`))
	fmt.Println("LOG AUTH-Manager/AccessKeys@Validate: done")
	return
}

//AuthenticateHandler ...
func AuthenticateHandler(ctx *fasthttp.RequestCtx) {

	ctx.SetContentType("application/json; charset=UTF-8")

	req, err := util.ParseUser(ctx.PostBody())

	if err != nil {

		ctx.SetBody([]byte(`{"message": "error reading request"}`))
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		return
	}

	usr, err := orm.IsUserValid(req)

	if err != nil {

		ctx.SetBody([]byte(`{"message": "Invalid user credentials"}`))
		ctx.SetStatusCode(fasthttp.StatusUnauthorized)
		return
	}

	orgs := orm.GetActiveOrgs(usr.UUID)

	mainToken, refreshToken, err := token.GenerateJWT(usr, false)

	if err != nil {

		ctx.SetBody([]byte(`{"message": "Error generating tokens"}`))
		ctx.SetStatusCode(fasthttp.StatusInternalServerError)
		return
	}

	rs := &token.TokenRS{
		Token:        mainToken,
		RefreshToken: refreshToken,
		CreatedAt:    time.Now().Format(time.RFC3339),
		Orgs:         orgs,
	}

	bt, err := json.Marshal(rs)
	if err != nil {

		ctx.SetBody([]byte(`{"message": "error encoding the response"}`))
		ctx.SetStatusCode(fasthttp.StatusInternalServerError)
	}

	tokenCookie := &fasthttp.Cookie{}
	refreshCookie := &fasthttp.Cookie{}
	tokenCookie.SetHTTPOnly(true)
	tokenCookie.SetKey("X-CMCSRF-TOKEN")
	tokenCookie.SetValue(mainToken)
	tokenCookie.SetPath("/")
	refreshCookie.SetHTTPOnly(true)
	refreshCookie.SetKey("X-CMCSRF-REFRESH-TOKEN")
	refreshCookie.SetValue(refreshToken)
	refreshCookie.SetPath("/")

	ctx.Response.Header.Add("X-CMDB", time.Now().String())
	ctx.Response.Header.SetCookie(tokenCookie)
	ctx.Response.Header.SetCookie(refreshCookie)
	ctx.SetStatusCode(fasthttp.StatusOK)
	ctx.SetBody(bt)
	fmt.Println("LOG AUTH-Manager/AccessKeys@Authenticate: done")
	return
}

//RefreshTokenHandler ...
func RefreshTokenHandler(ctx *fasthttp.RequestCtx) {

	ctx.SetContentType("application/json; charset=UTF-8")

	jwtInCookie := string(ctx.Request.Header.Cookie("X-CMCSRF-REFRESH-TOKEN"))
	jwtInHeader := string(ctx.Request.Header.Peek("X-CMA-TOKEN"))

	if jwtInCookie == "" || jwtInHeader == "" {

		ctx.SetBody([]byte(`{"message": "error reading request"}`))
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		return

	}

	claim, err := token.ValidateJWT(jwtInHeader, jwtInCookie)

	if err != nil {

		ctx.SetBody([]byte(`{"message": "Refresh token invalid or expired"}`))
		ctx.SetStatusCode(fasthttp.StatusUnauthorized)
		return
	}

	usr := &usr.User{
		UUID: claim.ID,
	}

	mainToken, refreshToken, err := token.GenerateJWT(usr, true)

	if err != nil {

		ctx.SetBody([]byte(`{"message": "Error generating tokens"}`))
		ctx.SetStatusCode(fasthttp.StatusInternalServerError)
		return
	}

	rs := &token.TokenRS{
		Token:        mainToken,
		RefreshToken: refreshToken,
		CreatedAt:    time.Now().Format(time.RFC3339),
	}

	bt, err := json.Marshal(rs)
	if err != nil {

		ctx.SetBody([]byte(`{"message": "error encoding the response"}`))
		ctx.SetStatusCode(fasthttp.StatusInternalServerError)
	}

	tokenCookie := &fasthttp.Cookie{}
	refreshCookie := &fasthttp.Cookie{}
	tokenCookie.SetHTTPOnly(true)
	tokenCookie.SetKey("X-CMCSRF-TOKEN")
	tokenCookie.SetValue(mainToken)
	tokenCookie.SetPath("/")
	refreshCookie.SetHTTPOnly(true)
	refreshCookie.SetKey("X-CMCSRF-REFRESH-TOKEN")
	refreshCookie.SetValue(refreshToken)
	refreshCookie.SetPath("/")

	ctx.Response.Header.Add("X-CMDB", time.Now().String())
	ctx.Response.Header.SetCookie(tokenCookie)
	ctx.Response.Header.SetCookie(refreshCookie)
	ctx.SetStatusCode(fasthttp.StatusOK)
	ctx.SetBody(bt)
	fmt.Println("LOG AUTH-Manager/AccessKeys@RefreshToken: done")
	return
}

//SelectOrgHandler ...
func SelectOrgHandler(ctx *fasthttp.RequestCtx) {

	ctx.SetContentType("application/json; charset=UTF-8")

	jwtInCookie := string(ctx.Request.Header.Cookie("X-CMCSRF-TOKEN"))
	jwtInHeader := string(ctx.Request.Header.Peek("X-CMA-TOKEN"))
	orgName := string(ctx.QueryArgs().Peek("org_name"))

	if jwtInCookie == "" || jwtInHeader == "" {

		ctx.SetBody([]byte(`{"message": "error selecting organisation"}`))
		ctx.SetStatusCode(fasthttp.StatusBadRequest)
		return

	}

	_, err := token.ValidateJWT(jwtInHeader, jwtInCookie)

	if err != nil {

		ctx.SetBody([]byte(`{"message": "Not valid auth"}`))
		ctx.SetStatusCode(fasthttp.StatusUnauthorized)
		return
	}

	schema, err := orm.SelectSchemaDB(orgName)

	if err != nil {

		ctx.SetBody([]byte(`{"message": "Organisation not found"}`))
		ctx.SetStatusCode(fasthttp.StatusNotFound)
		return
	}

	ctx.Response.Header.Add("X-CMO-DB", schema)
	ctx.SetStatusCode(fasthttp.StatusOK)
	ctx.SetBody([]byte(`{"message": "organisation selected successfully"}`))
	fmt.Println("LOG AUTH-Manager/AccessKeys@SelectOrg: done")
	return
}
