package token

import (
	"crypto/rsa"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"time"

	"github.com/dgrijalva/jwt-go"

	usr "gitlab.com/cloudmicro/platform/microservices/natural-born-users/iam-models.git/pkg/user"
)

var (
	signKey   *rsa.PrivateKey
	verifyKey *rsa.PublicKey
)

func readCertFiles() {

	if signKey == nil || verifyKey == nil {
		signBytes, err := ioutil.ReadFile(os.Getenv("AUT_MGR_RSA_PRIVATE_KEY"))
		if err != nil {
			fmt.Println("error reading the private cert file", err)
		}

		signKey, err = jwt.ParseRSAPrivateKeyFromPEM(signBytes)
		if err != nil {
			fmt.Println("error parsing jwt RSA private", err)
		}

		verifyBytes, err := ioutil.ReadFile(os.Getenv("AUT_MGR_RSA_PUBLIC_KEY"))
		if err != nil {
			fmt.Println("error reading the public cert file", err)
		}

		verifyKey, err = jwt.ParseRSAPublicKeyFromPEM(verifyBytes)
		if err != nil {
			fmt.Println("Error parsing jwt RSA public", err)
		}

	}
}

//ValidateJWT ...
func ValidateJWT(headerToken string, cookieToken string) (*ClaimToken, error) {
	readCertFiles()
	if headerToken != cookieToken {

		fmt.Println("Error valdiating token cookie and header are different")
		return nil, errors.New("Error valdiating token cookie and header are different")
	}

	verifyFunction := func(token *jwt.Token) (interface{}, error) {
		return verifyKey, nil
	}

	token, err := jwt.ParseWithClaims(headerToken, &ClaimToken{}, verifyFunction)
	if err != nil {
		fmt.Println("Error parsing JWT token", err)
		return nil, errors.New("Error validating token")
	}

	if !token.Valid {
		fmt.Println("token expired")
		return nil, errors.New("token has expired")
	}

	return token.Claims.(*ClaimToken), nil
}

//GenerateJWT ...
// Returns token string, refresh_token string, err error
func GenerateJWT(user *usr.User, refreshed bool) (string, string, error) {
	readCertFiles()
	claim := &ClaimToken{
		ID:        user.UUID,
		TokenType: "main",
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 3).Unix(),
			IssuedAt:  time.Now().Unix(),
			Issuer:    "auth-manager",
		},
	}

	tk := jwt.NewWithClaims(jwt.SigningMethodRS256, claim)
	token, err := tk.SignedString(signKey)
	if err != nil {

		fmt.Println("error signing main token", err)
		return "", "", err
	}

	if !refreshed {

		//time enough to refresh the token
		claim.ExpiresAt = time.Now().Add(time.Hour * 4).Unix()
		claim.TokenType = "refresh"

	} else {
		claim.TokenType = "refresh"
	}

	rtk := jwt.NewWithClaims(jwt.SigningMethodRS256, claim)
	refreshToken, err := rtk.SignedString(signKey)
	if err != nil {

		fmt.Println("error signing refresh token", err)
		return "", "", err
	}

	return token, refreshToken, nil
}

//RefreshToken ...
func RefreshToken(refreshToken string, cookieToken string, user *usr.User) (string, string, error) {

	if refreshToken != cookieToken {

		fmt.Println("Error valdiating token cookie and header are different")
		return "", "", errors.New("Error valdiating token cookie and header are different")

	}

	if _, err := ValidateJWT(refreshToken, cookieToken); err != nil {

		fmt.Println("token not valid or expired", err)
		return "", "", errors.New("token not valid or expired")

	}

	return GenerateJWT(user, true)
}
