package token

//TokenRS ...
type TokenRS struct {
	Token        string   `json:"token"`
	RefreshToken string   `json:"refresh_token"`
	CreatedAt    string   `json:"created_at"`
	Orgs         []string `json:"organisations"`
}
