package orm

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"time"

	//postgres driver
	_ "github.com/lib/pq"

	"cloudmicro/platform/auth-manager/internal/util"

	usr "gitlab.com/cloudmicro/platform/microservices/natural-born-users/iam-models.git/pkg/user"
)

//ValidateAccessTokens ...
func ValidateAccessTokens(accessKey string, secretKey string) error {

	conn, err := GetDBInstance()

	if err != nil {

		fmt.Println("Error connecting with dabase", err)
		return fmt.Errorf("Error creating connection with the database %v", err)
	}

	defer conn.Close()

	var schema string
	orgName := util.GetOrgName(accessKey)

	if err := conn.QueryRowContext(context.Background(), GetSchemaDbByNameQuery, orgName).Scan(&schema); err != nil {

		fmt.Println("Error validating organisation", err)
		return errors.New("Organisation not valid")
	}

	var uuid, status string
	var expiresAt sql.NullString

	if err := conn.QueryRowContext(context.Background(), util.ParseQuerySchema(schema, GetAccessKeysQuery), accessKey, secretKey).Scan(&uuid, &status, &expiresAt); err != nil {

		fmt.Println("Error getting access keys", err)
		return errors.New("Access and Secret Key not valid")
	}

	if !expiresAt.Valid {

		fmt.Println("Access keys are not valid since expiration is not set")
		return errors.New("Access and Secret Key not valid")
	}

	expiresAtParsed, err := time.Parse(time.RFC3339, expiresAt.String)

	if err != nil {

		fmt.Println(err)
		return errors.New("Error formating expiration time")
	}

	if time.Now().After(expiresAtParsed) == true || status != "active" {

		fmt.Println("Access keys has expired or are not active, need to review the credentials", expiresAtParsed, status)
		return errors.New("Access and Secret Key expired or are not active")
	}

	if _, err := conn.ExecContext(context.Background(), util.ParseQuerySchema(schema, UpdateLastUsedKey), uuid); err != nil {

		fmt.Println("Error updating last used access keys")
	}

	return nil
}

//IsUserValid ...
func IsUserValid(user *usr.User) (*usr.User, error) {

	conn, err := GetDBInstance()

	if err != nil {

		fmt.Println("Error connecting with dabase", err)
		return nil, fmt.Errorf("Error creating connection with the database %v", err)
	}

	defer conn.Close()

	userRs := &usr.User{}
	var userSecretHash string

	if err := conn.QueryRowContext(context.Background(), GetValidUserQuery, user.UserLogin).Scan(&userRs.UUID, &userSecretHash); err != nil {

		fmt.Println("Error validating user:", err)
		return nil, errors.New("Error validating user credentials")

	}

	if !util.CompareHash(userSecretHash, user.UserSecret) {

		fmt.Println("password invalid")
		return nil, errors.New("password invalid")
	}

	return userRs, nil
}

//GetActiveOrgs ...
func GetActiveOrgs(userUUID string) []string {

	var rs []string
	conn, err := GetDBInstance()

	if err != nil {

		fmt.Println("Error connecting with dabase", err)
		return rs
	}

	defer conn.Close()

	row, err := conn.QueryContext(context.Background(), GetActiveOrgsQuery, userUUID)

	var org string
	for row.Next() {

		row.Scan(&org)
		rs = append(rs, org)
	}

	if err != nil {

		fmt.Println("Error getting active organisations user:", err)

	}

	return rs
}

//SelectSchemaDB ...
func SelectSchemaDB(orgName string) (string, error) {

	conn, err := GetDBInstance()

	if err != nil {

		fmt.Println("Error connecting with dabase", err)
		return "", fmt.Errorf("Error creating connection with the database %v", err)
	}

	defer conn.Close()

	var schema string

	if err := conn.QueryRowContext(context.Background(), GetSchemaDbByNameQuery, orgName).Scan(&schema); err != nil {

		fmt.Println("Error validating organisation name:", err)
		return "", errors.New("Error validating organisation name")

	}

	return schema, nil
}
