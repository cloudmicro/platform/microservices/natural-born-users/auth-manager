package util

import (
	"encoding/base64"
	"errors"
	"fmt"
	"os"

	"golang.org/x/crypto/bcrypt"
)

var hashSalt = os.Getenv("HASH_SALT")

//SecretHash ...
func SecretHash(secret string) (string, error) {

	hs, err := bcrypt.GenerateFromPassword([]byte(hashSalt+secret), bcrypt.MinCost)

	if err != nil {

		fmt.Println("error creating hash secret", err)
		return "", errors.New("error creating hash secret")
	}

	return string(hs), nil
}

//CompareHash ...
func CompareHash(hash string, secret string) bool {

	if err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(hashSalt+secret)); err != nil {

		fmt.Println("Password does not match with hash", err)
		return false
	}
	return true
}

//GetOrgName ...
func GetOrgName(accessKey string) string {

	if accessKey == "" {
		return ""
	}

	if len(accessKey) < 24 {
		return ""
	}

	preName := accessKey[10:]
	name := preName[:len(preName)-10]

	result, err := base64.StdEncoding.DecodeString(name)

	if err != nil {

		fmt.Println("error decoding orgname", err)
		return ""
	}

	return string(result)
}
