package util

import (
	"encoding/json"
	"fmt"
	"strings"

	usr "gitlab.com/cloudmicro/platform/microservices/natural-born-users/iam-models.git/pkg/user"
)

//ParseUser ...
func ParseUser(payload []byte) (*usr.User, error) {

	request := &usr.User{}

	if err := json.Unmarshal(payload, request); err != nil {

		fmt.Println("Err: error decoding user request", err)
		return nil, err
	}

	return request, nil
}

//ParseQuerySchema ...
func ParseQuerySchema(schema string, query string) string {

	return strings.Replace(query, "#*", schema, -1)
}
