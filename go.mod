module cloudmicro/platform/auth-manager

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/kr/pretty v0.1.0 // indirect
	github.com/lib/pq v1.2.0
	github.com/valyala/fasthttp v1.6.0
	gitlab.com/cloudmicro/platform/microservices/cm-http.git v1.0.4
	gitlab.com/cloudmicro/platform/microservices/natural-born-users/iam-models.git v1.0.1
	golang.org/x/crypto v0.0.0-20190308221718-c2843e01d9a2
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
)
